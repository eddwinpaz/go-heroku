# Deploy Go code to Heroku with GitLab CI

An exemplary deployment pipeline from GitLab to Heroku for Go applications.

Automatically triggered on push/merge:

- [x] golint
- [x] go tool vet
- [x] go test -race
- [x] deploy to staging (automatically)
- [x] deploy to production (manually)

## How-To

1. Create a new Heroku Pipeline.

    ![New Pipeline](_images/new_pipeline.png)

2. Set up as many Heroku apps in this pipeline as you need (e.g. staging and production).

    ![Pipeline](_images/pipeline.png)

3. `git clone` this repository or copy whatever you need.

4. Get your Heroku API key found in your [account](https://dashboard.heroku.com/account).

    ![API Key](_images/api_key.png)

5. Set the key as the variable `HEROKU_API_KEY` in your GitLab  [CI / CD Settings](https://gitlab.com/sj14/go-heroku/settings/ci_cd).

    ![CI/CD Variables](_images/ci_variables.png)

6. Modify `.gitlab-ci.yml` and change the existing app name (`-app=gitlab-heroku-deploy-*`) to your newly created apps.

## Hints

- Heroku uses the [$PORT](https://devcenter.heroku.com/articles/dynos#local-environment-variables) environment variable (see `main.go`)
- In this setup, Heroku knows what to deploy based on the modified `go.mod` file (see [buildpack](https://elements.heroku.com/buildpacks/heroku/heroku-buildpack-go) for alternatives):

    ```text
    // +heroku install .
    ```

    This is not required here, and `.` is the default, but when your main file is in a different folder, you have to give the corresponding path instead of the `.`.

- The `Procfile` decides which command should be run. Again, in this setup it's not required as the binary is the same as the project name. When you have to run another command, change the command after `web:` accordingly ([The Procfile](https://devcenter.heroku.com/articles/procfile)).

    ```text
    web: go-heroku
    ```

## Older approach using godep

See the [using-godep](https://gitlab.com/sj14/go-heroku/tree/using-godep) branch for an example using `dep` instead of go modules.

## Further Reading

- Inspired by Stacy Goh: [Setting up CI/CD on Gitlab](https://hackernoon.com/setting-up-ci-cd-on-gitlab-step-by-step-guide-part-1-826385728223) (using node)
- GitLab CI/CD [Examples](https://docs.gitlab.com/ce/ci/examples/README.html)
- Also inspiring and more complete, using a Makefile but without Heroku: [Go tools and GitLab: How to do continuous integration like a boss](https://about.gitlab.com/2017/11/27/go-tools-and-gitlab-how-to-do-continuous-integration-like-a-boss/)